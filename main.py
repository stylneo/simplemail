import json
from simple_mail import SimpleMail

if __name__ == '__main__':
	with open("simple_mail_config.json", "r") as f:
		config = json.load(f)

	mail = SimpleMail(
		"origin_mail@example.com",
		"dest_mail@example.com",
		"test subject",
		"this is my payload!",
		config,
	)
	mail.send()
"""
Simple module for personal use to whenever I have something that needs to send 
out emails.
Basically a wrapper for stmplib and email and a way for me not to have to lookup 
how to do it everytime I need it.
"""

import smtplib
from email.message import EmailMessage

"""
Config is a dicionary with the following fields:

Config= {
	'user': <username> # The user to auth in the STMP server
	'password': <password> # The password to auth in the STMP server
	'server': <server-address> # Address of the STMP server.
}

You can figure out how to populate it securely.
"""


class SimpleMail:
    def __init__(
        self,
        from_address: str,
        to_address: str,
        subject: str,
        message: str,
        config: dict[str, str],
    ) -> None:
        self.from_adress = from_address
        self.to_address = to_address
        self.subject = subject
        self.message = message
        self.config = config
        self.__compose_mail()

    def __compose_mail(self) -> None:
        self.email = EmailMessage()
        self.email["From"] = self.from_adress
        self.email["To"] = self.to_address
        self.email["Subject"] = self.subject
        self.email.set_content(self.message)

    def send(self) -> None:
        with smtplib.SMTP(self.config["server"]) as s:
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login(self.config["user"], self.config["password"])
            s.send_message(self.email)

